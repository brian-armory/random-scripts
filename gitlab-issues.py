#!/usr/bin/env python3

# You can create the script manually or clone the script and use it script via a symlink:
# For example `ln -s /Users/brian/rando-scripts/gitlab-issues.py /usr/local/bin/gli`
# Make sure you create an environment variable named REPO_URL for your terminal, such as .zshenv. Set the env variable equal to https://gitlab.com/<org|username>/<repo>/-/issues
# Comment out the pyperclip stuff if you don't want to install it. It's only used for reading the clipboard if you run the script without providing an issue number.

import webbrowser, sys, pyperclip, os
# repo_url = os.environ['REPO_URL']
repo_url = "https://gitlab.com/brian-armory/random-scripts/-/issues/"
sys.argv

if len(sys.argv) > 1:  
  issue_number = ' '.join(sys.argv[1:])
else:  
  issue_number = pyperclip.paste()

webbrowser.open_new_tab(repo_url + issue_number)


